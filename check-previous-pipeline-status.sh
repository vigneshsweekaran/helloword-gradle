#!/bin/bash

GITLAB_URL=https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines
PREVIOUS_PIPELINE_ID=$(curl -s --header "PRIVATE-TOKEN:$pass" "$GITLAB_URL" | jq -r .[0].id); echo $PREVIOUS_PIPELINE_ID

while [ $(curl -s --header "PRIVATE-TOKEN:$TOKEN" "$GITLAB_URL/$PREVIOUS_PIPELINE_ID/jobs" | jq -r '.[] | select(.stage=="build-scp") | .status') == "running" ]; do echo "Previous pipeline build-scp stage is running, checking the status every 1 minute"; sleep 60; done
